FROM golang:alpine AS main-env
RUN mkdir /app
ADD . /app/
WORKDIR /app
RUN cd /app && go build -o scratchpay

FROM alpine
WORKDIR /app
COPY --from=main-env /app/scratchpay /app
EXPOSE 8080

ENTRYPOINT ./scratchpay
