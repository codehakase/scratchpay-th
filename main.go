package main

import (
	"codehakase/scratchpay-th/clinic"
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	var (
		host = flag.String("http", ":3000", "http listen address")
		ctx  = context.Background()
	)

	flag.Parse()

	// register service
	clinicSrv := clinic.New()

	// register transport
	mux := http.NewServeMux()
	mux.Handle("/clinics", clinic.NewHTTPServer(ctx, clinicSrv))
	http.Handle("/", mux)

	errs := make(chan error, 2)

	go func() {
		log.Printf("Running Server on port=%v", *host)
		errs <- http.ListenAndServe(*host, nil)
	}()

	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errs <- fmt.Errorf("%s", <-c)
	}()

	log.Printf("terminated: %v", <-errs)
}
