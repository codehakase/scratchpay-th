# Scratchpay-th

### Organization
- `clinic` reprensents the clinic service with the `Search` functionality
- `main.go` is the entry point of the Search api.

## Running & Building application
Download dependencies with:
```shell
$ go mod tidy && go mod vendor
```

Run application with:
```shell
$ go run main.go
```

Build application with:
```shell
$ go build -o scratchpay-th main.go
```

Dockerize with:
```shell
$ docker build -t codehakase/scratchpay-th .
```

## Running Tests
In the root of the application, run `go test ./...` to run all test suites



## Service API Definitions
**Search Clinics**

**Definitions**

`GET /clinics`

**Params**
- `name:string`
- `state:string`
- `availability:string` eg: `from:09:00,to:20:00`

**Response**
- `200 OK` on success
```json
{
    "code": 200,
    "data": [
        {
            "availability": {
                "from": "10:00",
                "to": "19:30"
            },
            "name": "Good Health Home",
            "stateName": "Alaska"
        },
        {
            "availability": {
                "from": "11:00",
                "to": "22:00"
            },
            "name": "UAB Hospital",
            "stateName": "Alaska"
        },
        {
            "clinicName": "Good Health Home",
            "opening": {
                "from": "15:00",
                "to": "20:00"
            },
            "stateCode": "FL"
        }
    ],
    "status": "sucess"
}
```
