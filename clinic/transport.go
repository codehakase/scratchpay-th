package clinic

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
)

// NewHTTPServer returns a list of configured routes for the clinic service
func NewHTTPServer(ctx context.Context, s Service) http.Handler {
	r := mux.NewRouter()

	r.HandleFunc("/clinics", searchEndpoint(ctx, s)).Methods("GET")
	return r
}

func searchEndpoint(ctx context.Context, s Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		vars := r.URL.Query()
		req := SearchClinicReq{
			Name:         vars.Get("name"),
			State:        vars.Get("state"),
			Availability: vars.Get("availability"),
		}
		resp, err := s.Search(ctx, req)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(map[string]interface{}{
			"status": "sucess",
			"code":   http.StatusOK,
			"data":   resp,
		})
	}
}
