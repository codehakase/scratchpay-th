package tests

import (
	"codehakase/scratchpay-th/clinic"
	"codehakase/scratchpay-th/clinic/mocks"
	"context"
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/mock"
)

var (
	testResponse []map[string]interface{}
)

// Test search with mocks
func TestSearch(t *testing.T) {
	setup(t)

	ctx := context.Background()
	srv := mocks.Service{}
	req := clinic.SearchClinicReq{Name: "hello"}
	srv.On("Search", ctx, mock.Anything).Return(testResponse, nil)

	results, err := srv.Search(ctx, req)
	if err != nil {
		t.Errorf("expected err to be nil, got %v", err)
	}

	if len(testResponse) != len(results) {
		t.Errorf("expected len results be %v got %v", len(testResponse), len(results))
	}
}

func TestSearchImpl(t *testing.T) {
	ctx := context.Background()
	srv := clinic.New()
	testcases := []struct {
		req         clinic.SearchClinicReq
		expectedLen int
	}{
		{clinic.SearchClinicReq{Name: "test"}, 2},
		{clinic.SearchClinicReq{Name: "good", State: "alaska"}, 3},
		{clinic.SearchClinicReq{Name: "good", State: "alaska", Availability: "from:09:00,to:20:00"}, 9},
	}

	for _, tt := range testcases {
		t.Run(fmt.Sprintf("Test for expected len=%v", tt.expectedLen), func(t *testing.T) {
			results, err := srv.Search(ctx, tt.req)
			if err != nil {
				t.Errorf("expected err to be nil, got %v", err)
			}

			if len(results) != tt.expectedLen {
				t.Errorf("expected len results be %v got %v", tt.expectedLen, len(results))
			}
		})
	}
}

func BenchmarkSearch(b *testing.B) {
	ctx := context.Background()
	srv := clinic.New()

	req := clinic.SearchClinicReq{Name: "test"}
	for n := 0; n < b.N; n++ {
		_, _ = srv.Search(ctx, req)
	}
}

func setup(t *testing.T) {
	err := json.Unmarshal([]byte(`
[
        {
            "availability": {
                "from": "10:00",
                "to": "19:30"
            },
            "name": "Good Health Home",
            "stateName": "Alaska"
        },
        {
            "availability": {
                "from": "11:00",
                "to": "22:00"
            },
            "name": "UAB Hospital",
            "stateName": "Alaska"
        }
	]
`), &testResponse)
	if err != nil {
		t.Fatal(err)
	}
}
