package clinic

// SearchClinicReq ...
type SearchClinicReq struct {
	Name         string
	State        string
	Availability string
}
