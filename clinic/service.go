package clinic

import (
	"context"
	"encoding/json"
	"net/http"
	"strings"
	"time"
)

var (
	dentalClinicsURL = "https://storage.googleapis.com/scratchpay-code-challenge/dental-clinics.json"
	vetClinicsURL    = "https://storage.googleapis.com/scratchpay-code-challenge/vet-clinics.json"
)

// Service reprensents the Clinic Service, exposing methods to access
// clinic data
type Service interface {
	Search(ctx context.Context, req SearchClinicReq) ([]map[string]interface{}, error)
}

// Srv implements the Service interface
type Srv struct {
}

// New creates a new Clinic Service
func New() Service {
	return &Srv{}
}

// Search queries the clinic datastores matching provided search params
func (s *Srv) Search(ctx context.Context, req SearchClinicReq) ([]map[string]interface{}, error) {
	resp := make([]map[string]interface{}, 0)

	dentialClinics, err := callEndpoint(dentalClinicsURL)
	if err != nil {
		return nil, err
	}

	vetClincs, err := callEndpoint(vetClinicsURL)
	if err != nil {
		return nil, err
	}

	// apply search filters to payloads
	dentalResults := s.processClinic(dentialClinics, req, "name", "stateName", "availability")
	vetResults := s.processClinic(vetClincs, req, "clinicName", "stateCode", "opening")

	resp = append(resp, dentalResults...)
	resp = append(resp, vetResults...)
	return resp, nil
}

func (s *Srv) processClinic(
	payload []map[string]interface{},
	req SearchClinicReq,
	nameArg, stateArg, availabilityArg string,
) []map[string]interface{} {
	results := make([]map[string]interface{}, 0)
	for _, dc := range payload {
		temp := make(map[string]interface{})
		if req.Name != "" && strContains(dc[nameArg].(string), req.Name) {
			temp = dc
		}

		if req.State != "" && strContains(dc[stateArg].(string), req.State) {
			temp = dc
		}

		if req.Availability != "" {
			if s.checkAvailability(dc, availabilityArg, req.Availability) {
				temp = dc
			}
		}

		if len(temp) > 0 {
			results = append(results, temp)
		}
	}
	return results
}

func (s *Srv) checkAvailability(d map[string]interface{}, field, query string) bool {
	availability := d[field].(map[string]interface{})
	curfrom := availability["from"].(string)
	curto := availability["to"].(string)
	pts := strings.Split(query, ",")
	if len(pts) < 2 {
		return false
	}

	layout := "03:04"
	from, _ := time.Parse(layout, pts[0][5:])
	to, _ := time.Parse(layout, pts[1][5:])
	rFrom, _ := time.Parse(layout, curfrom)
	rTo, _ := time.Parse(layout, curto)

	// check queried availability range is within set
	if inTimeSpan(rFrom, rTo, from) && inTimeSpan(rFrom, rTo, to) {
		return true
	}
	return false
}

func strContains(s, substr string) bool {
	s, substr = strings.ToUpper(s), strings.ToUpper(substr)
	return strings.Contains(s, substr)
}

// https://stackoverflow.com/questions/55093676/checking-if-current-time-is-in-a-given-interval-golang
func inTimeSpan(start, end, check time.Time) bool {
	if start.Before(end) {
		return !check.Before(start) && !check.After(end)
	}
	if start.Equal(end) {
		return check.Equal(start)
	}
	return !start.After(check) || !end.Before(check)
}

func callEndpoint(e string) ([]map[string]interface{}, error) {
	client := &http.Client{
		Timeout: time.Second * 10,
	}
	req, err := http.NewRequest("GET", e, nil)
	if err != nil {
		return nil, err
	}

	req.Close = true
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var data []map[string]interface{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return nil, err
	}

	return data, nil
}
